const https = require("https"),
    http = require("http"),
    qs = require('querystring'),
    mysql = require('mysql');
class playlistmaker {
    pm(artist) {
        let pretty = '';

        if (artist==null){
            return;
        }


        if (artist.includes("+")){
            artist = artist.replace("+", " ")
        }
        let makerSongArr = [];

        let playlistID = 0;
        let spotifypy_id ='';

        //Make a draft playlist so I could get back a playlist id;
        let DraftOption = {
            host: '54.185.140.188',
            path: '/maker/api/draftPlaylist',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'token': 'cbc5bfb8-7ae6-4e08-a4b0-05d17c171be7'
            }
        };


        let body1 = {
            "playlistName": artist + " Playlist",
            "notes": "",
            "songs": []
        };
        let output4 = '';
        let req4 = http.request(DraftOption, function(res4) {
            console.log(DraftOption.host + ':' + res4.statusCode);
            console.log('STATUS: ' + res4.statusCode);
            console.log('HEADERS: ' + JSON.stringify(res4.headers));
            res4.setEncoding('utf8');

            res4.on('data', function (chunk) {
                console.log('DATA: ' + chunk);
                output4 += chunk;
                let obj4 = JSON.parse(output4);
                playlistID = obj4.data;
                //console.log(obj4.data);

            });
        });
        req4.on('error', function(e) {
            console.log('problem with request: ' + e.message);
        });
        req4.write(JSON.stringify(body1));
        req4.end();



        let userInput = artist;
        if (userInput.includes(" ")){
            userInput = userInput.replace(/\s+/g, "+")
        }
        let MAKeRoptions = {
            host: '54.185.140.188',
            path: '/maker/api/searchSong?searchString='+ userInput + '&pageNo=0&rowSize=20',
            method: 'GET',
            headers: {
                'token': 'cbc5bfb8-7ae6-4e08-a4b0-05d17c171be7'
            }
        };

        // it searches for the artist that the user entered
        let req3 = http.request(MAKeRoptions, function(res3)
        {
            let output3 = '';
            console.log(MAKeRoptions.host + MAKeRoptions.path+':' + res3.statusCode);
            console.log('STATUS: ' + res3.statusCode);
            // console.log('HEADERS: ' + JSON.stringify(res3.headers));
            // res3.setEncoding('utf8');

            res3.on('data', function (chunk) {
                console.log('DATA: ' + chunk);
                output3 += chunk;
            });

            res3.on('end', function() {
                let obj3 = JSON.parse(output3);
                let index = 0;
                if (obj3.data.length < 5) {
                    pretty = 'Oh, bummer. We couldn\'t make a playlist for this Artist. Try again.';

                    req3.end();
                    return pretty;
                }
                else if (obj3.data.length > 5) {
                    for (index=0; index<obj3.data.length; index++) {
                        let s = obj3.data[index].songName;
                        let a = obj3.data[index].artistName;
                        // check
                        if (a.includes(artist)) {
                            // output the whole data information of the song
                            console.log(obj3.data[index]);

                            makerSongArr.push(obj3.data[index]);
                        }
                    }
                }


                if (makerSongArr.length < 3) {
                    pretty = 'Oh, bummer. We couldn\'t make a playlist for this Artist. Try again.';
                    return pretty;
                }

                let body2 = {
                    "playlistName": artist + " Playlist",
                    "notes": "",
                    "songs": makerSongArr
                };
                console.log(JSON.stringify(body2));
                let output5 = '';
                let SubmitOption = {
                    host: '54.185.140.188',
                    path: '/maker/api/submitPlaylist?id='+playlistID,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'token': 'cbc5bfb8-7ae6-4e08-a4b0-05d17c171be7'
                    }
                };
                let req5 = http.request(SubmitOption, function(res5) {
                    console.log(SubmitOption.host + ':' + res5.statusCode);
                    console.log('STATUS: ' + res5.statusCode);
                    console.log('HEADERS: ' + JSON.stringify(res5.headers));
                    res5.setEncoding('utf8');

                    res5.on('data', function (chunk) {
                        console.log('DATA: ' + chunk);
                        output5 += chunk;
                        let obj5 = JSON.parse(output5);
                        //console.log(obj4.data);

                    });
                });
                req5.on('error', function(e) {
                    console.log('problem with request: ' + e.message);
                });
                req5.write(JSON.stringify(body2));

                req5.end();
            });
        });
        req3.end();
        setTimeout(()=>{
            let connection = mysql.createConnection({
                host     : '54.185.140.188',
                user     : 'root',
                password : 'P!@yL#&t@19Jun#2018',
                database : 'playlist'
            });

            connection.connect();

            connection.query('SELECT spotify_playlist_id from playlist where id='+playlistID+';', function (error, results, fields) {
                if (error) {
                    console.log('The solution is: ', error);
                    throw error;
                }
                else{

                    pretty = 'Listen to ' + artist +' Playlist at https://open.spotify.com/playlist/'+results[0].spotify_playlist_id;

                    console.log(pretty+'im last ');
                    connection.end();
                    return pretty;
                }
            });

        }, 1000);
    }

}


//console.log(pm('James+Arthur'));
export default new playlistmaker();